const express = require('express')
const http = require('http')
const { Server } = require("socket.io")
const mongoose = require('mongoose')
const Logger = require('./src/Logger')

mongoose
    .connect('mongodb://172.17.0.3:27017/chat', {
        user: 'admin',
        pass: 'admin'
    })
    .then(Logger.debug('Database connected successfully'))

const app = express()
const server = http.createServer(app)
const io = new Server(server)

const { body, validationResult } = require('express-validator')

const { Article } = require('./src/Schemas/Article')
const { Request } = require('./src/Schemas/Request')
const { Message } = require('./src/Schemas/Message')

app.use(express.json()) 

app.put(
    '/api/article/:article',

    body('title').isString().isLength({min: 5}).optional({nullable: true}), // rules for validation
    body('content').isString().isLength({min: 10}).optional({nullable: true}), // too

    async (req, res) => {
        // validate user request body
        const errors = validationResult(req)

        try {
            // Throw when validation errors in the body
            errors.throw()
        } catch {
            // Catch errors and send validation errors to the client
            return res.status(400).json({ errors: errors.array() })
        }

        const { article } = req.params
        const { title, content } = req.body

        await Article.updateOne({
            _id: article
        }, {
            title,
            content,
            updatedAt: Date.now()
        })

        res.sendStatus(202)
    }
)

app.get('/api/article', async (req, res) => {
    Logger.infoRequest(req)

    const articles = await Article.find()

    res.send(articles)
})

app.get('/api/article/:article', async (req, res) => {
    Logger.infoRequest(req)

    try {
        const article = await Article.findById(req.params.article)

        res.send(article)
    } catch {
        res.status(404)
		res.send({ error: "Post doesn't exist!" })
    }
})

app.post(
    '/api/article',

    body('title').isString().isLength({min: 5}), // rules for validation
    body('content').isString().isLength({min: 10}), // too

    async (req, res) => {
        Logger.infoRequest(req)

        // validate user request body
        const errors = validationResult(req)

        try {
            // Throw when validation errors in the body
            errors.throw()
        } catch {
            // Catch errors and send validation errors to the client
            return res.status(400).json({ errors: errors.array() })
        }

        const article = new Article({
            title: req.body.title,
            content: req.body.content,
        })

        await article.save()

        res.send(article)
    }
)

app.get('/', async (req, res) => {
    Logger.infoRequest(req)

    const request = new Request({
        ip: req.ip,
        path: req.path,
        hostname: req.hostname,
        lang: req.acceptsLanguages()
    })

    request.save().then(() => Logger.debug('[MONGOOSE] request saved ' + request._id))

    res.sendFile(__dirname + '/resources/view/index.html')
})

app.get('/chat', async (req, res) => {
    Logger.infoRequest(req)

    const request = new Request({
        ip: req.ip,
        path: req.path,
        hostname: req.hostname,
        lang: req.acceptsLanguages()
    })

    request.save().then(() => Logger.debug('[MONGOOSE] request saved ' + request._id))

    res.sendFile(__dirname + '/resources/view/chat.html')
})

io.on('connection', async (socket) => {
    Logger.debug('[IO] new socket connection ' + socket.id)

    const messages = await Message.find({
            'date': { 
                $exists: true 
            }
        })
        .limit(10)
        .sort('-date')

        Logger.debug('[MONGOOSE] retrieved ' + messages.length + ' messages from database for socket ' + socket.id)
        
        messages.forEach((message) => {
            Logger.debug('[IO] send message to ' + socket.id + 'socket from ' + message.socket + ' socket')

            socket.emit("chat message", {
                username: message.username,
                message: message.chat || message.message,
            })
        })

    socket.on('chat message', value => {
        Logger.debug('[IO] new message from ' + socket.id + ' socket')

        const message = new Message({
            username: value.username,
            message: value.message,
            date: Date.now(),
            socket: socket.id,
            ip: socket.handshake.address,
            headers: socket.request.headers
        })

        message.save()
            .then(() => Logger.debug('[MONGOOSE] message saved ' + message._id))

        socket.broadcast.emit('chat message', message)
    })

    socket.on('disconnect', () => {
        Logger.debug('[IO] socket disconnected ' + socket.id)
    })
})

server.listen(5000, () => {
    Logger.info('[HTTP] Server start on port 5000')
})